$('document').ready(function(){
    $("#clients").owlCarousel({
        center:true,
        items: 2,
        loop: true,
        mouseDrag: false,
        nav: true,

        responsive:{
            500: {
                loop: false,
                center: false,
                items: 4
            }
        }
    });
    $("#testimonials").owlCarousel({
        center:true,
        items: 1,
        loop: true,
        mouseDrag: false,
        nav: true,
        dotsContainer: $('.testimonials__btn-wrapper'),
        responsive:{
            500: {
                loop: false,
                center: false,
                items: 2
            }
        }
    });
    $('#flowers').owlCarousel({
        center:true,
        items: 2,
        margin: 10,
        rewind: true,
        loop: true,
        mouseDrag: false,
        nav: true,
        itemClass: 'flower_item',
        autoWidth:true
    });

    var firstBrick = $('.firstBrick'),
        secondBrick = $('.secondBrick'),
        thirdBrick = $('.thirdBrick');

    (function(){
        var tick = 0;
        var maxTicks = 4;
            setInterval(function() {
                tick === maxTicks ? tick = 0 : tick === tick;
                switch (tick){
                    case 0:firstBrick.addClass('first__pair');
                        tick++;
                        break;
                    case 1:secondBrick.addClass('second__pair');
                        tick++;
                        break;
                    case 2:thirdBrick.addClass('third__pair');
                        tick++;
                        break;
                    case 3:firstBrick.removeClass('first__pair');
                        secondBrick.removeClass('second__pair');
                        thirdBrick.removeClass('third__pair');
                        tick++;
                        break;
                }
            }, 2000)
    }());
    (function(){
        var tick = 0;
        var maxTicks = 5;
        setInterval(function() {
            tick === maxTicks ? tick = 0 : tick === tick;
            switch (tick){
                case 0:$('.step_one').addClass('red__circle');
                    tick++;
                    break;
                case 1:$('.step_two').addClass('red__circle');
                    tick++;
                    break;
                case 2:$('.step_three').addClass('red__circle');
                    tick++;
                    break;
                case 3:$('.step_four').addClass('red__circle');
                    tick++;
                    break;
                case 4:$('.step__number').removeClass('red__circle')
                    tick++;
                    break;
            }
        }, 1000)
    }());

});